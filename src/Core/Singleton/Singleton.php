<?php

declare(strict_types=1);

namespace MeTools\Core\Singleton;

use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Exception\InfrastructureException;

abstract class Singleton
{
    private static array $instances = [];

    protected function __construct() {}

    protected function __clone() {}

    public function __wakeup()
    {
        throw new InfrastructureException('Cannot unserialize a singleton.', ErrorCode::INTERNAL_ERROR);
    }

    public static function getInstance(): static
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }
        return self::$instances[$cls];
    }
}