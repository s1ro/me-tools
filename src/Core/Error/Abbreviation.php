<?php

declare(strict_types=1);

namespace MeTools\Core\Error;

use MeTools\Core\Error\Exception\InvalidAbbreviationException;
use MeTools\Core\ValueObject\ValueObject;

class Abbreviation extends ValueObject
{
    public const UNDEFINED_ERROR_ABBREVIATION = 'undefined';
    public const ABBREVIATION_PATTERN = '/^\b[a-zA-Z0-9_\.\-]+\b$/';

    private string $abbreviation;

    protected function __construct(string $abbreviation)
    {
        $abbreviation = trim($abbreviation);
        if ('' === $abbreviation) {
            throw InvalidAbbreviationException::emptyAbbreviation();
        }
        if (!preg_match(self::ABBREVIATION_PATTERN, $abbreviation)) {
            throw InvalidAbbreviationException::invalidFormat($abbreviation);
        }
        $this->abbreviation = $abbreviation;
    }

    public static function make(string $abbreviation): static
    {
        return new static($abbreviation);
    }

    public static function undefined(): static
    {
        return new static(self::UNDEFINED_ERROR_ABBREVIATION);
    }

    public function get(): string
    {
        return $this->abbreviation;
    }
}