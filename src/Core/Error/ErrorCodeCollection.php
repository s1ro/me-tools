<?php

declare(strict_types=1);

namespace MeTools\Core\Error;

use MeTools\Core\Collection\Collection;

class ErrorCodeCollection extends Collection
{
    protected ?string $allowedType = ErrorCode::class;

    public static function fromScalarArray(array $codes): self
    {
        $errorCodes = new static();
        foreach ($codes as $code) {
            $errorCodes->add(ErrorCode::make((int)$code));
        }

        return $errorCodes;
    }

    public function contain(ErrorCode $errorCode): bool
    {
        /** @var ErrorCode $existCode */
        foreach ($this->items as $existCode) {
            if ($existCode->isEqualWith($errorCode)) {
                return true;
            }
        }
        return false;
    }
}