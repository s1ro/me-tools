<?php

declare(strict_types=1);

namespace MeTools\Core\Error\Exception;

use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Error\ErrorMessage;
use MeTools\Core\Exception\ValidationException;

class InvalidMessageException extends ValidationException
{
    public static function emptyMessage(): static
    {
        return new static('Error message can\'t be empty.', ErrorCode::BAD_REQUEST);
    }

    public static function greaterThanMax(string $gotMessage): static
    {
        $gotMessageLength = mb_strlen($gotMessage);
        $errorMessage = sprintf(
            'Error message can\'t be greater than %s. Got with length %s',
            ErrorMessage::MAX_LENGTH,
            $gotMessage
        );

        return new static($errorMessage, ErrorCode::BAD_REQUEST);
    }
}