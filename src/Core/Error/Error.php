<?php

declare(strict_types=1);

namespace MeTools\Core\Error;

class Error
{
    private ErrorCode $code;
    private Abbreviation $abbreviation;
    private ErrorMessage $message;

    public function __construct(ErrorCode $code, Abbreviation $abbreviation, ErrorMessage $message)
    {
        $this->code = $code;
        $this->abbreviation = $abbreviation;
        $this->message = $message;
    }

    public static function undefined(): static
    {
        return new static(
            ErrorCode::make(ErrorCode::INTERNAL_ERROR),
            Abbreviation::undefined(),
            ErrorMessage::undefined()
        );
    }

    public static function fromArray(array $error): static
    {
        return new static(
            ErrorCode::make($error['code']),
            Abbreviation::make($error['abbreviation']),
            ErrorMessage::make($error['message'])
        );
    }

    public function getCode(): ErrorCode
    {
        return $this->code;
    }

    public function getAbbreviation(): Abbreviation
    {
        return $this->abbreviation;
    }

    public function getMessage(): ErrorMessage
    {
        return $this->message;
    }

    public function asArray(): array
    {
        return [
            'code' => $this->code->get(),
            'abbreviation' => $this->abbreviation->get(),
            'message' => $this->message->get(),
        ];
    }
}