<?php

declare(strict_types=1);

namespace MeTools\Core\Error;

use MeTools\Core\Error\Exception\InvalidMessageException;
use MeTools\Core\ValueObject\ValueObject;

class ErrorMessage extends ValueObject
{
    public const UNDEFINED_ERROR_MESSAGE = 'Some error occurred. Contact support please.';
    public const MAX_LENGTH = 500;

    private string $errorMessage;

    protected function __construct(string $errorMessage)
    {
        $errorMessage = trim($errorMessage);
        if ('' === $errorMessage) {
            throw InvalidMessageException::emptyMessage();
        }
        if (mb_strlen($errorMessage) > self::MAX_LENGTH) {
            throw InvalidMessageException::greaterThanMax($errorMessage);
        }
        $this->errorMessage = $errorMessage;
    }

    public static function make(string $errorMessage): static
    {
        return new static($errorMessage);
    }

    public static function undefined(): static
    {
        return new static(self::UNDEFINED_ERROR_MESSAGE);
    }

    public function get(): string
    {
        return $this->errorMessage;
    }
}