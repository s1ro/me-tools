<?php

declare(strict_types=1);

namespace MeTools\Core\Error;

use MeTools\Core\ValueObject\Enum;

class ErrorCode extends Enum
{
    public const BAD_REQUEST = 400;
    public const ACCESS_DENIED = 403;
    public const NOT_FOUND = 404;
    public const INTERNAL_ERROR = 500;
}