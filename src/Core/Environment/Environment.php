<?php

declare(strict_types=1);

namespace MeTools\Core\Environment;

use MeTools\Core\ValueObject\Enum;

class Environment extends Enum
{
    public const DEV = 'dev';
    public const TEST = 'test';
    public const PROD = 'prod';
}