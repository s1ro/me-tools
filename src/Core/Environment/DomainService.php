<?php

declare(strict_types=1);

namespace MeTools\Core\Environment;

use MeTools\Core\Singleton\Singleton;

class DomainService extends Singleton
{
    private ?string $domainWithHttp = null;
    private ?string $domain = null;

    public function getDomain(): string
    {
        if (!$this->domain) {
            $this->domain = $_SERVER['HTTP_HOST'];
        }
        return $this->domain;
    }

    public function getDomainWithHttp(): string
    {
        if (!$this->domainWithHttp) {
            $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] === 443)?
                'https://' : 'http://';
            $this->domainWithHttp = $protocol . $this->getDomain();
        }
        return $this->domainWithHttp;
    }
}