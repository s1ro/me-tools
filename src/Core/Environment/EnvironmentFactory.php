<?php

declare(strict_types=1);

namespace MeTools\Core\Environment;

use MeTools\Core\ValueObject\Exception\InvalidPathException;

class EnvironmentFactory
{
    private Environment $environment;
    private ProjectPath $projectDir;

    /**
     * @throws InvalidPathException
     */
    public function __construct(string $environment, string $projectDir)
    {
        $this->environment = Environment::make($environment);
        $this->projectDir = ProjectPath::make($projectDir);
        if ($this->projectDir->isFile()) {
            throw InvalidPathException::notDir();
        }
    }

    public function environment(): Environment
    {
        return $this->environment;
    }

    public function projectPath(): ProjectPath
    {
        return $this->projectDir;
    }

    // TODO: Add hosts from DomainService
}