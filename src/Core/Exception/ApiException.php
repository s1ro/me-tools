<?php

declare(strict_types=1);

namespace MeTools\Core\Exception;

use MeTools\Core\Error\ErrorCollection;

class ApiException extends AppException
{
    private ?ErrorCollection $errors = null;

    public function getErrors(): ErrorCollection
    {
        return $this->errors ?? new ErrorCollection();
    }

    public static function make(string $message, ErrorCollection $errors): static
    {
        $apiException = new static($message);
        $apiException->errors = $errors;

        return $apiException;
    }

    public static function fromThrowable(\Throwable $t): static
    {
        return new static($t->getMessage(), $t->getCode(), $t);
    }
}