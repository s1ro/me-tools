<?php

declare(strict_types=1);

namespace MeTools\Core\Exception;

class ValidationException extends \InvalidArgumentException
{

}