<?php

declare(strict_types=1);

namespace MeTools\Core\Exception;

use MeTools\Core\Error\ErrorCode;

class InfrastructureException extends RuntimeException
{
    public static function fromUnexpectedThrowable(\Throwable $t): static
    {
        return new static(
            sprintf('Unexpected exception: %s', $t->getMessage()),
            ErrorCode::INTERNAL_ERROR,
            $t
        );
    }

    public static function transactionFailure(?\Throwable $previus = null): static
    {
        return new static(
            'Process transaction was finish with failure.',
            ErrorCode::INTERNAL_ERROR,
            $previus
        );
    }
}