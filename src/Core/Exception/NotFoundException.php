<?php

namespace MeTools\Core\Exception;

use MeTools\Core\Error\ErrorCode;

class NotFoundException extends AppException
{
    public function __construct(string $message = "", ?\Throwable $previous = null)
    {
        parent::__construct($message, ErrorCode::NOT_FOUND, $previous);
    }

    public static function fromThrowable(\Throwable $t): static
    {
        return new static($t->getMessage(), $t);
    }
}