<?php

declare(strict_types=1);

namespace MeTools\Core\ValueObject\Exception;

use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Exception\ValidationException;

class InvalidEmailException extends ValidationException
{
    public static function emptyEmail(): static
    {
        return new static('Email can\'t be empty.', ErrorCode::BAD_REQUEST);
    }

    public static function invalidFormat(string $email): static
    {
        return new static(
            sprintf('Invalid email format. Got email "%s"', $email),
            ErrorCode::BAD_REQUEST
        );
    }
}