<?php

declare(strict_types=1);

namespace MeTools\Core\ValueObject\Exception;

use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Exception\ValidationException;

class InvalidTokenException extends ValidationException
{
    public static function emptyToken(): static
    {
        return new static('Token can\'t be empty.', ErrorCode::BAD_REQUEST);
    }
}