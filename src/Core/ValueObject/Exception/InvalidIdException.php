<?php

declare(strict_types=1);

namespace MeTools\Core\ValueObject\Exception;

use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Exception\ValidationException;
use MeTools\Core\ValueObject\Id;

class InvalidIdException extends ValidationException
{
    public static function lessThanMin(): static
    {
        return new static(
            sprintf('Id can\'t be less than %s', Id::MIN_VALUE),
            ErrorCode::BAD_REQUEST
        );
    }
}