<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Validation\Token;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class TokenConstraint extends Constraint
{

}