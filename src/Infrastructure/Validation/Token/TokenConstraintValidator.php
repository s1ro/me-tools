<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Validation\Token;

use MeTools\Core\ValueObject\Exception\InvalidTokenException;
use MeTools\Core\ValueObject\Token;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class TokenConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof TokenConstraint) {
            throw new UnexpectedTypeException($constraint, TokenConstraint::class);
        }
        try {
            Token::make((string)$value);
        } catch (InvalidTokenException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}