<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Validation\Email;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class EmailConstraint extends Constraint
{

}