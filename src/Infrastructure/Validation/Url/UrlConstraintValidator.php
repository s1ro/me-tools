<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Validation\Url;

use MeTools\Core\ValueObject\Exception\InvalidUrlException;
use MeTools\Core\ValueObject\Url;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UrlConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof UrlConstraint) {
            throw new UnexpectedTypeException($constraint, UrlConstraint::class);
        }
        try {
            Url::make((string)$value);
        } catch (InvalidUrlException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}