<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Validation\Url;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class UrlConstraint extends Constraint
{

}