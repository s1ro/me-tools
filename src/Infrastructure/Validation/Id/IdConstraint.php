<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Validation\Id;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class IdConstraint extends Constraint
{

}