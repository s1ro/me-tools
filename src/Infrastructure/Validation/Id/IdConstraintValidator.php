<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Validation\Id;

use MeTools\Core\ValueObject\Exception\InvalidIdException;
use MeTools\Core\ValueObject\Id;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class IdConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof IdConstraint) {
            throw new UnexpectedTypeException($constraint, IdConstraint::class);
        }
        try {
            Id::make((int)$value);
        } catch (InvalidIdException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}