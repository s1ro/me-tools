<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Validation\Phone;

use MeTools\Core\Exception\InvalidPhoneNumberException;
use MeTools\Core\ValueObject\Phone;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class PhoneConstraintValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof PhoneConstraint) {
            throw new UnexpectedTypeException($constraint, PhoneConstraint::class);
        }
        try {
            Phone::fromString((string)$value);
        } catch (InvalidPhoneNumberException $e) {
            $this->context->buildViolation($e->getMessage())
                ->setCode((string)$e->getCode())
                ->addViolation();
        }
    }
}