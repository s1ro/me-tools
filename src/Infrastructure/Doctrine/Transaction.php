<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Doctrine;

use Doctrine\ORM\EntityManagerInterface;
use MeTools\Core\Exception\InfrastructureException;

class Transaction
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function begin(): void
    {
        try {
            $isSuccessfullyStarted = $this->entityManager->getConnection()->beginTransaction();
        } catch (\Throwable $t) {
            throw InfrastructureException::transactionFailure($t);
        }
        if (!$isSuccessfullyStarted) {
            throw InfrastructureException::transactionFailure();
        }
    }

    public function commit(): void
    {
        try {
            $isSuccessfullyCommit = $this->entityManager->getConnection()->commit();
        } catch (\Throwable $t) {
            throw InfrastructureException::transactionFailure($t);
        }
        if (!$isSuccessfullyCommit) {
            throw InfrastructureException::transactionFailure();
        }
    }

    public function rollBack(): void
    {
        try {
            $isSuccessfullyRollback = $this->entityManager->getConnection()->rollBack();
        } catch (\Throwable $t) {
            throw InfrastructureException::transactionFailure($t);
        }
        throw InfrastructureException::transactionFailure();
    }
}