<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Doctrine\CustomType;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use MeTools\Core\ValueObject\Url;

class UrlType extends Type
{
    private const NAME = 'url';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return 'VARCHAR(2000)';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Url
    {
        if (is_null($value)) {
            return null;
        }
        return Url::make($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (is_null($value)) {
            return null;
        }
        return $value->get();
    }
}