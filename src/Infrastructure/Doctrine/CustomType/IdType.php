<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Doctrine\CustomType;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use MeTools\Core\ValueObject\Id;

class IdType extends Type
{
    private const NAME = 'id';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return 'INT UNSIGNED';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Id
    {
        return Id::make((int)$value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): int
    {
        /** @var Id $value */
        return $value->get();
    }
}