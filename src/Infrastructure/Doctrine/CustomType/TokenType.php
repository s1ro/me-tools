<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\Doctrine\CustomType;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use MeTools\Core\ValueObject\Token;

class TokenType extends Type
{
    private const NAME = 'token';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return 'VARCHAR(255)';
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): Token
    {
        return Token::make($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        /** @var Token $value */
        return $value->get();
    }
}