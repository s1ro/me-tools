<?php

declare(strict_types=1);

namespace MeTools\Infrastructure\EventSubscriber;

use MeTools\Core\Error\Abbreviation;
use MeTools\Core\Error\Error;
use MeTools\Core\Error\ErrorCode;
use MeTools\Core\Error\ErrorMessage;
use MeTools\Core\Exception\ApiException;
use MeTools\Core\Exception\InfrastructureException;
use MeTools\Core\Exception\NotFoundException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $t = $event->getThrowable();
        if ($t instanceof ApiException) {
            $previous = $t->getPrevious();
            $responseCode = $this->detectResponseCode($event->getRequest(), $t);
            $errors = $t->getErrors();
            if ($errors->isEmpty() && !is_null($previous)) {
                $errorCode = ErrorCode::assertInList($previous->getCode())
                    ? ErrorCode::make($previous->getCode())
                    : ErrorCode::make(ErrorCode::INTERNAL_ERROR);
                $errors->add(new Error($errorCode, Abbreviation::undefined(), ErrorMessage::make($previous->getMessage())));
            }
            if ($errors->isEmpty()) {
                $errors->add(Error::undefined());
            }
            $response = new JsonResponse(['errors' => $errors->asArray()], $responseCode);
            $event->setResponse($response);
        }

    }

    private function detectResponseCode(Request $request, ApiException $exception): int
    {
        $previous = $exception->getPrevious();
        $responseCode = match (true) {
            $exception->getErrors()->count() > 0 => Response::HTTP_BAD_REQUEST,
            is_null($previous) || $previous instanceof InfrastructureException => Response::HTTP_INTERNAL_SERVER_ERROR,
            $previous instanceof AccessDeniedException => Response::HTTP_FORBIDDEN,
            $previous instanceof NotFoundException || $previous->getCode() === ErrorCode::NOT_FOUND => Response::HTTP_NOT_FOUND,
            default => Response::HTTP_BAD_REQUEST,
        };
        return $responseCode;
    }
}