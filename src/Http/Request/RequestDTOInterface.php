<?php

declare(strict_types=1);

namespace MeTools\Http\Request;

use Symfony\Component\HttpFoundation\Request;

interface RequestDTOInterface
{
    public function __construct(Request $request);

    public function getOriginalRequest(): Request;
}