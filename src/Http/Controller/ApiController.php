<?php

namespace MeTools\Http\Controller;

use MeTools\Core\Exception\ApiException;
use MeTools\Http\Request\RequestDTOInterface;
use MeTools\Http\Request\RequestValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class ApiController extends AbstractController
{
    public function __construct(private RequestValidator $requestValidator) {}

    protected function getRequestValidator(): RequestValidator
    {
        return $this->requestValidator;
    }

    protected function validateRequest(RequestDTOInterface $requestDTO): void
    {
        $errors = $this->requestValidator->validateRequest($requestDTO);
        if ($errors->count() > 0) {
            throw ApiException::make('Validation Error', $errors);
        }
    }
}